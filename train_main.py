# Copyright (C) tuyenple. All rights reserved.
import os
from basd.runner import build_and_train
from basd.utils.core_utils import get_yml_content
# from basd.rewarders.base_rewarder import BaseRewarder


# sodaflow tracking
from omegaconf import DictConfig, OmegaConf
import sodaflow


# class Reward(BaseRewarder):
#     def __init__(self, **kwargs):
#         super(Reward, self).__init__(**kwargs)

#     def compute_reward(self, predicted_action, target=None, metrics=None, **kwargs):
#         # DUMMY REWARD FUNCTION. THIS CAN BE CUSTOMIZED BY LG-CNS
#         return 0.001 * (metrics['qlt_1']['value'] + metrics['qlt_2']['value'] + metrics['qlt_3']['value']) / 3

def _is_basd_config(key):
    basd_configs = [
        'builtinAlgorithm', 
        'simulator_url', 
        'simulator_port', 
        'episode_length', 
        'eval_max_steps', 
        'eval_max_trajectories'
    ]
    if key in basd_configs:
        return True
    else:
        return False


@sodaflow.main(
    trial_name='local_training'
)
def run_app(cfg: DictConfig) -> None:
    import psutil
    import random
    from pprint import pprint
    #add tracking for BakingSoDA_report.docx, progress.csv log
    from sodaflow import tracking
    
    try:
        train_config = OmegaConf.to_container(cfg)

        trial_file_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "./configs/DDPG_config.yaml"))
        trial_params = get_yml_content(trial_file_path)

        # merge train config
        train_inputs = train_config['train_inputs']
        for k, v in train_inputs.items():
            if not _is_basd_config(k):
                trial_params['parameters']['hyper'][k] = v

        trial_params['representation']['agent']['builtinAlgorithm'] = train_inputs['builtinAlgorithm']
        trial_params['datasource']['train_args']['host'] = train_inputs['simulator_url']
        trial_params['datasource']['train_args']['port'] = train_inputs['simulator_port']
        trial_params['datasource']['train_args']['episode_length'] = train_inputs['episode_length']
        trial_params['datasource']['test_args']['host'] = train_inputs['simulator_url']
        trial_params['datasource']['test_args']['port'] = train_inputs['simulator_port']
        trial_params['datasource']['test_args']['episode_length'] = train_inputs['episode_length']
        trial_params['datasource']['test_args']['eval_max_steps'] = train_inputs['eval_max_steps']
        trial_params['datasource']['test_args']['eval_max_trajectories'] = train_inputs['eval_max_trajectories']

        # cpu resource setting
        if os.environ.get('CPU_COUNT'):
            cpu_list = list(range(psutil.cpu_count()))
            choice = random.sample(cpu_list, int(os.environ['CPU_COUNT']))
            choice.sort()
            os.environ['CPU_VISIBLE_DEVICES'] = ','.join([str(item) for item in choice])

        cpu_visible_devices = os.environ.get(
                "CPU_VISIBLE_DEVICES",
                trial_params.get("resources", dict()).get("CPU_VISIBLE_DEVICES", None),
            )

        print(f'cpu_visible_devices -> {cpu_visible_devices}')

        # temp code (for lgcns cpu mode)
        env_cuda = os.environ.get('CUDA_VISIBLE_DEVICES')
        if env_cuda is not None:
            if not env_cuda:
                print(f"VISIBLE_DEVICE -> [{os.environ.get('CUDA_VISIBLE_DEVICES')}]")
                del os.environ['CUDA_VISIBLE_DEVICES']
                trial_params['resources']['CUDA_VISIBLE_DEVICES'] = None

        cuda_visible_devices = os.environ.get(
                "CUDA_VISIBLE_DEVICES",
                trial_params.get("resources", dict()).get("CUDA_VISIBLE_DEVICES", None),
            )
        print(f'gpu_visible_devices -> {cuda_visible_devices}')

        pprint(trial_params)
        build_and_train(trial_params) 
        print('Reward All Done...')
        # #add BakingSoDA_report.docx,progress.csv in log
        # tracking.log_model(os.path.join(os.environ['BASD_OUTPUT_DIR'],"progress.csv"))
        # tracking.log_model(os.path.join(os.environ['BASD_OUTPUT_DIR'],"BakingSoDA_report.docx"))
    except Exception as e:
        raise e


if __name__ == '__main__':
    os.environ['BASD_OUTPUT_DIR'] = './output'
    run_app()        

