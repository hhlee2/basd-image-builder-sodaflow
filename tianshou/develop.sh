#!/bin/sh

# 현재 경로 저장
TIANSHOU_PATH=`pwd`
echo $TIANSHOU_PATH

# 라이브러리로 이동
# /home/agilesoda/anaconda3/envs/basd/lib/python3.8/site-packages\

#PYTHON_PACKAGE_PATH=$(python -c "import site; print(''.join(site.getsitepackages()))")
PYTHON_PACKAGE_PATH='/home/coder/.local/lib/python3.7/site-packages/'
echo $PYTHON_PACKAGE_PATH

# 경로 이동
cd $PYTHON_PACKAGE_PATH

echo $(pwd)

# 기존 폴더 제거
rm -rf tianshou

# mkdir basd
# mkdir basd_cmd

ln -s $TIANSHOU_PATH/tianshou ./tianshou

# install black
yes | pip install black
conda install -c conda-forge -y pre_commit

# install pre-commit for git hook
yes | pip install pre-commit
conda install -c conda-forge -y pre_commit

cd $TIANSHOU_PATH

# 기본 패키지들 설치
# pip install -r ./requirements_develop.txt

# install git hook script
pre-commit install

# set git message
git config commit.template .gitmessage