import numpy as np


class Reward:
    def __init__(self, **kwargs):
        super(Reward, self).__init__(**kwargs)

    def compute_reward(self, predicted_action, target=None, metrics=None, **kwargs):
        done = metrics["done"]["value"]
        steps_beyond_done = metrics["steps_beyond_done"]["value"]

        if not done:
            reward = 1.0
        elif steps_beyond_done is None:
            reward = 1.0
        else:
            reward = 0.0

        return reward
