import numpy as np


def angle_normalize(x):
    return ((x + np.pi) % (2 * np.pi)) - np.pi


class Reward:
    def __init__(self, **kwargs):
        super(Reward, self).__init__(**kwargs)

    def compute_reward(self, predicted_action, target=None, metrics=None, **kwargs):

        #######################################
        ########### pendulum ##################
        #######################################
        th = metrics["th"]["value"]
        thdot = metrics["thdot"]["value"]
        u = predicted_action["u"]
        costs = angle_normalize(th) ** 2 + 0.1 * thdot**2 + 0.001 * (u**2)
        return -costs
