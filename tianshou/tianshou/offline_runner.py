from cgi import parse_multipart
from curses import noecho
import os
from ssl import ALERT_DESCRIPTION_UNSUPPORTED_CERTIFICATE
import numpy as np
import gym
import yaml
from typing import Dict, Any, Union
import torch
from torch.distributions import Independent, Normal
from tianshou.data import Collector, VectorReplayBuffer, AsyncCollector
from tianshou.data.buffer.prio import PrioritizedReplayBuffer

from tianshou.env.remote_env import RemoteEnv
from tianshou.env.venvs import DummyVectorEnv
from tianshou.exploration.random import GaussianNoise
from tianshou.policy.modelfree.dqn import DQNPolicy
from tianshou.policy.modelfree.ppo import PPOPolicy
from tianshou.policy.modelfree.ddpg import DDPGPolicy
from tianshou.policy.modelfree.td3 import TD3Policy
from tianshou.trainer.onpolicy import OnpolicyTrainer
from tianshou.trainer.offpolicy import OffpolicyTrainer
from tianshou.utils.net.common import ActorCritic, Net, DataParallelNet

USING_SODAFLOW = False
try:
    from sodaflow import tracking

    USING_SODAFLOW = True
except Exception:
    pass

## 2022-06-16 기록 : 에러 관련은 1차로 assert로 하고 추후 Logg따로.. 개발자용 로그랑 사용자용 로그 따로 만들긴 해야함


def train_offline(trial_params: Dict[str, Any]):
    raise NotImplementedError
    # load conf
    datasource = trial_params.get("datasource", None)

    assert datasource is not None, "datasource is None"

    representation_info = trial_params.get("representation", None)

    assert representation_info is not None, "representation is None"

    parameter_info = trial_params.get("parameter", None)

    assert parameter_info is not None, "parameter is None"

    ## set Environment
    action_type = None
    if mode_RL == "online":
        # Online
        ## set Environment
        subtype = datasource.get("subtype", None)
        env_params = dict()
        env_params["reward_info"] = representation_info.get("reward", None)
        if subtype == "remote":
            # host, port, episode 정보 읽기
            env_params["host"] = datasource.get("host", None)
            env_params["port"] = datasource.get("port", None)
            env_params["episode_length"] = datasource.get("episode_length", None)

            env = RemoteEnv(**env_params)
            ### State Info
            ### Action Info
            state_shape = env.observation_space.shape or env.observation_space.n
            action_shape = env.action_space.shape or env.action_space.n
            action_type = env.action_type
            max_action = (
                env.action_space.high[0] if action_type == "continuous" else None
            )
            ### make train, test envs
            training_num = 10
            test_num = 1
            # train_envs = DummyVectorEnv(
            #     [lambda: RemoteEnv(**env_params) for _ in range(training_num)]
            # )
            train_envs = DummyVectorEnv(
                [lambda: gym.make("Pendulum-v1") for _ in range(training_num)]
            )
            # test_envs = gym.make(args.task)
            test_envs = DummyVectorEnv(
                [lambda: gym.make("Pendulum-v1") for _ in range(test_num)]
            )

        else:
            raise NotImplementedError
    elif mode_RL == "offline":
        # environment info 받기
        # buffer에 데이터 넣기
        pass

    else:
        raise NotImplementedError

    ## set seed
    seed = parameter_info.get("seed", 1234)
    np.random.seed(seed)
    torch.manual_seed(seed)
    train_envs.seed(seed)
    test_envs.seed(seed)

    ## set model
    ### Net params
    net_params = dict()
    net_params["state_shape"] = state_shape
    net_params["action_shape"] = action_shape

    hidden_sizes_info = parameter_info.get("hidden_sizes", "128/128")
    net_params["hidden_sizes"] = convert_hidden_size_shape(hidden_sizes_info)
    norm_layer = parameter_info.get("norm_layer", False)
    net_params["norm_layer"] = torch.nn.LayerNorm if norm_layer else None
    activation = parameter_info.get("activation", "ReLU")
    net_params["activation"] = convert_params_to_activation(
        activation, net_params["hidden_sizes"]
    )
    device = parameter_info.get("device", "cpu")
    if device == "cuda":
        is_multi_gpu = parameter_info.get("multi_gpu", False)
    net_params["softmax"] = parameter_info.get("softmax", False)
    net_params["num_atoms"] = parameter_info.get("num_atoms", 1)
    net_params["concat"] = False
    #### dueling_param
    use_dueling = parameter_info.get("dueling_param").get("use_dueling", False)
    if use_dueling:
        # 추후 구현.
        # 구현 형태는 아마 위에 net 설정한거를
        # func화 해서 dueling Net에도 사용할 수 있도록
        raise NotImplementedError
    else:
        net_params["dueling_param"] = None
    net = Net(**net_params)

    agent_info = representation_info.get("agent", None)
    algorithm = agent_info.get("algorithm")

    learning_rate = parameter_info.get("learning_rate")
    actor_learning_rate = parameter_info.get("actor_learning_rate", learning_rate)
    critic_learning_rate = parameter_info.get("critic_learning_rate", learning_rate)

    if action_type == "discrete":
        if algorithm == "PPO":
            from tianshou.utils.net.discrete import Actor, Critic

            actor = Actor(net, action_shape, device=device).to(device)
            critic = Critic(net, device=device).to(device)
            if torch.cuda.is_available():
                actor = DataParallelNet(actor)
                critic = DataParallelNet(critic)
            actor_critic = ActorCritic(actor, critic)
            for m in actor_critic.modules():
                if isinstance(m, torch.nn.Linear):
                    torch.nn.init.orthogonal_(m.weight)
                    torch.nn.init.zeros_(m.bias)

            dist = torch.distributions.Categorical

        elif algorithm == "DQN":
            pass

    elif action_type == "continuous":
        if algorithm == "PPO":
            from tianshou.utils.net.continuous import ActorProb, Critic

            actor = ActorProb(
                net, action_shape, max_action=max_action, device=device
            ).to(device)
            critic = Critic(net, device=device).to(device)
            actor_critic = ActorCritic(actor, critic)
            # orthogonal initialization
            for m in actor_critic.modules():
                if isinstance(m, torch.nn.Linear):
                    torch.nn.init.orthogonal_(m.weight)
                    torch.nn.init.zeros_(m.bias)

            dist = dist_independent

        elif algorithm == "DDPG":
            from tianshou.utils.net.continuous import Actor, Critic

            actor = Actor(net, action_shape, max_action=max_action, device=device).to(
                device
            )
            net_params["concat"] = True
            net = Net(**net_params)
            critic = Critic(net, device=device).to(device)

        elif algorithm == "TD3":
            from tianshou.utils.net.continuous import Actor, Critic

            actor = Actor(net, action_shape, max_action=max_action, device=device).to(
                device
            )
            net_params["concat"] = True
            net = Net(**net_params)
            critic = Critic(net, device=device).to(device)
            critic2 = Critic(net, device=device).to(device)
    else:
        assert False, "action_type is wrong"

    ## set Optim
    optimCls = convert_str_to_optimCls(parameter_info.get("optimizer"))
    if algorithm in ["PPO", "A2C", "PG"]:
        optimizer = optimCls(actor_critic.parameters(), lr=learning_rate)
    elif algorithm in ["DDPG", "TD3"]:
        actor_optim = torch.optim.Adam(actor.parameters(), lr=actor_learning_rate)
        critic_optim = torch.optim.Adam(critic.parameters(), lr=critic_learning_rate)
        if algorithm == "TD3":
            critic2_optim = torch.optim.Adam(
                critic2.parameters, lr=critic_learning_rate
            )
    else:
        optimizer = optimCls(net.parameters(), lr=learning_rate)

    ## read parameter
    policy_parameter = get_policy_parameter(algorithm, parameter_info)

    ## set Policy

    if algorithm == "PPO":
        policy = PPOPolicy(
            actor,
            critic,
            optimizer,
            dist,
            action_space=env.action_space,
            **policy_parameter,
        )
    elif algorithm == "DQN":
        policy = DQNPolicy(
            net,
            optimizer,
            **policy_parameter,
        )
    elif algorithm == "DDPG":
        policy = DDPGPolicy(
            actor,
            actor_optim,
            critic,
            critic_optim,
            exploration_noise=GaussianNoise(sigma=0.1),
            action_space=env.action_space,
            **policy_parameter,
        )
    elif algorithm == "TD3":
        policy = TD3Policy(
            actor,
            actor_optim,
            critic,
            critic_optim,
            critic2,
            critic2_optim,
            exploration_noise=GaussianNoise(sigma=0.1),
            action_space=env.action_space,
            **policy_parameter,
        )
    else:
        raise NotImplementedError

    ## set buffer if needed

    ## set Collector
    buffer_size = parameter_info.get("buffer_size")
    use_prioritized_replay = parameter_info.get("use_prioritized_replay", False)
    buffer_alpha = parameter_info.get("buffer_alpha", 0.5)
    buffer_beta = parameter_info.get("buffer_beta", 0.5)

    if algorithm == "DQN" and use_prioritized_replay:
        buf = PrioritizedReplayBuffer(
            buffer_size, len(train_envs), buffer_alpha, buffer_beta
        )
    else:
        buf = VectorReplayBuffer(buffer_size, len(train_envs))

    train_collector = Collector(policy, train_envs, buf, exploration_noise=True)
    # test_collector = Collector(policy, test_envs, exploration_noise=True)
    test_collector = Collector(policy, test_envs)

    # train_collector = AsyncCollector(
    #     policy, train_envs, VectorReplayBuffer(buffer_size, len(train_envs))
    # )
    # test_collector = AsyncCollector(policy, test_envs)

    log_path = "ppo"

    ## make function for saving policy
    ## make function for stopping policy
    ## make function for checkpint
    def save_best_fn(policy):
        torch.save(policy.state_dict(), os.path.join(log_path, "policy.pth"))
        if USING_SODAFLOW:
            tracking.log_model(os.path.join(log_path, "policy.pth"))

    ## load checkpoint if resume

    ## init Trainer
    epoch = parameter_info.get("epoch")
    step_per_epoch = parameter_info.get("step_per_epoch")
    step_per_collect = parameter_info.get("step_per_collct")
    repeat_per_collect = parameter_info.get("repeat_per_collect")
    batch_size = parameter_info.get("batch_size")
    episode_per_collect = parameter_info.get("episode_per_collect")
    episode_per_test = parameter_info.get("episode_per_test")
    update_per_step = parameter_info.get("update_per_step")

    if algorithm in ["A2C", "PPO", "PG"]:
        trainer = OnpolicyTrainer(
            policy,
            train_collector,
            test_collector,
            epoch,
            step_per_epoch,
            repeat_per_collect,
            test_num,
            batch_size=batch_size,
            episode_per_collect=episode_per_collect,
        )
    elif algorithm in ["DQN"]:

        def train_fn(epoch, env_step):
            # eps annnealing, just a demo
            if env_step <= 10000:
                policy.set_eps(0.1)
            elif env_step <= 50000:
                eps = 0.1 - (env_step - 10000) / 40000 * (0.9 * 0.1)
                policy.set_eps(eps)
            else:
                policy.set_eps(0.1 * 0.1)

        trainer = OffpolicyTrainer(
            policy,
            train_collector,
            test_collector,
            epoch,
            step_per_epoch,
            step_per_collect,
            episode_per_test,
            train_fn=train_fn,
            batch_size=batch_size,
            update_per_step=update_per_step,
            episode_per_collect=episode_per_collect,
        )
    elif algorithm in ["DDPG", "TD3"]:
        trainer = OffpolicyTrainer(
            policy,
            train_collector,
            test_collector,
            epoch,
            step_per_epoch,
            step_per_collect,
            episode_per_test,
            batch_size=batch_size,
            update_per_step=update_per_step,
            episode_per_collect=episode_per_collect,
        )

    print("Set finish, Start Train")
    for epoch, epoch_stat, info in trainer:
        print(f"Epoch: {epoch}")
        print(epoch_stat)
        print(info)
        if USING_SODAFLOW:
            tracking.log_outputs()
    # Offline
    ## check data exist
    ## data load
    ## set state conf (shape, dim)
    ## set action conf (shape, dim)
    ## make test envs
    ## set seed
    ## set model
    ## set optim
    ## load policy if resume
    ## set test Collector

    ## make function for saving policy
    ## make function for stopping policy
    ## make function for checkpint

    # train with iter and print train result
    return


def convert_hidden_size_shape(hidden_size_info: str) -> list:
    hidden_size = []
    split_word = hidden_size_info.split("/")
    for n_nodes in split_word:
        hidden_size.append(int(n_nodes))
    return hidden_size


def convert_params_to_activation(
    acti_parmas: Union[str, list], hidden_size: list
) -> Union[torch.nn.Module, list]:
    if isinstance(acti_parmas, list):
        activation = []
        for acti_str in acti_parmas:
            activation.append(convert_str_to_module(acti_str))
        return activation
    elif isinstance(acti_parmas, str):
        return convert_str_to_module(acti_parmas)
    return None


def convert_str_to_module(activation: str) -> torch.nn.Module:
    ####
    # reference : https://pytorch.org/docs/stable/nn.html#non-linear-activations-weighted-sum-nonlinearity
    ####
    activation_str = activation.lower()
    if activation_str == "elu":
        return torch.nn.ELU
    elif activation_str == "leakyrelu":
        return torch.nn.LeakyReLU
    elif activation_str == "relu":
        return torch.nn.ReLU
    elif activation_str == "sigmoid":
        return torch.nn.Sigmoid
    elif activation_str == "tanh":
        return torch.nn.Tanh
    else:
        return None


def convert_str_to_optimCls(optim_str: str) -> torch.optim:
    optimCls = None
    optim_str = optim_str.lower()
    if optim_str == "adam":
        optimCls = torch.optim.Adam
    # elif optim_str == "":
    #     optimCls =

    return optimCls


def dist_independent(*logits):
    return Independent(Normal(*logits), 1)


def get_policy_parameter(algorithm: str, parameter_info: Dict) -> Dict:
    policy_parameter = None
    # for make dictionary
    class DictClass(object):
        pass

    mydict = DictClass()

    if algorithm == "PPO":
        mydict.discount_factor = parameter_info.get("discount_factor", 0.9)
        mydict.max_grad_norm = parameter_info.get("max_grad_norm")
        mydict.eps_clip = parameter_info.get("epsilon_clip", None)
        mydict.vf_coef = parameter_info.get("value_func_coef")
        mydict.ent_coef = parameter_info.get("entropy_coef")
        mydict.reward_normalization = parameter_info.get("reward_normalization")
        mydict.advantage_normalization = parameter_info.get(
            "advantage_normalization", None
        )
        mydict.recompute_advantage = parameter_info.get("recompute_advantage", None)
        mydict.dual_clip = parameter_info.get("dual_clip")
        mydict.value_clip = parameter_info.get("value_clip")
        mydict.gae_lambda = parameter_info.get("gae_lambda")
    elif algorithm == "DQN":
        mydict.estimation_step = parameter_info.get("n_step", 1)
        mydict.target_update_freq = parameter_info.get("target_update_freq", 0)
        mydict.reward_normalization = parameter_info.get("reward_normalization", False)
        mydict.is_double = parameter_info.get("is_double", True)
        mydict.clip_loss_grad = parameter_info.get("clip_loss_grad", False)
    elif algorithm == "DDPG" or algorithm == "TD3":
        mydict.tau = parameter_info.get("tau", 0.005)
        mydict.reward_normalization = parameter_info.get("reward_normalization", False)
        mydict.estimation_step = parameter_info.get("estimation_step", 1)
        mydict.action_scaling = parameter_info.get("action_scaling", True)
        if algorithm == "TD3":
            mydict.policy_noise = parameter_info.get("policy_noise", 0.2)
            mydict.update_actor_freq = parameter_info.get("update_actor_freq", 2)
            mydict.noise_clip = parameter_info.get("noise_clip", 0.5)
    elif algorithm == "SAC":
        pass
    elif algorithm == "TRPO":
        pass
    elif algorithm == "A2C":
        pass
    elif algorithm == "PG":
        pass
    elif algorithm == "RAINBOW_DQN":
        pass

    policy_parameter = mydict.__dict__

    return policy_parameter


def get_parameter_config(file_path: str) -> Dict:
    """
    Load yaml file content

    Args:
        file_path (str) : yml file path
    Return:
        experiment_config (dict) : config information

    """
    with open(file_path, "r") as file:
        experiment_config = yaml.load(file, Loader=yaml.Loader)

    return experiment_config


if __name__ == "__main__":
    try:
        file_path = "/home/david/tianshou/examples/bakingsoda/test_config_td3.yaml"
        trial_params = get_parameter_config(file_path)
        train(trial_params)

    except Exception as e:
        raise e
