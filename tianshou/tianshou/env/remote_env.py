import random
import time

import gym
import numpy as np
import json
from gym.spaces import Box, Discrete
from dataclasses import dataclass


from tianshou.env.utils import create_customized_class_instance, append_to_metric_info
from tianshou.utils.simulator.remote_utils import (
    check_simulator_server,
    get_info_url,
    reset_simulator_url,
    new_simulator_url,
    step_simulator_url,
    set_seed_url,
    close_simulator_url,
)
from tianshou.utils.simulator.remote_utils import rest_post, rest_get


@dataclass
class ColumnInfo:
    names: np.array
    types: np.array


class RemoteEnv(gym.Env):
    def __init__(self, host, port, episode_length, reward_info, **kwargs):
        super().__init__()

        self._host = host
        self._port = port

        self._episode_length = episode_length

        data = dict()
        data["kwargs"] = kwargs

        response = rest_post(
            new_simulator_url(host=host, port=port),
            data=json.dumps(data),
            timeout=120,
            show_error=True,
        )

        if response is None:
            raise Exception("Cannnot get basic info")
        response_data = json.loads(response.text)
        self._sim_id = response_data.get("sim_id")

        running, _ = check_simulator_server(host, port)
        if not running:
            raise Exception("Simulator does not run now!")

        response = rest_get(get_info_url(host, port), 5, show_error=True)
        if response is None:
            raise Exception("Cannot get basic info")
        response_data = json.loads(response.text)

        self.action_type = response_data.get("action_type")
        action_info = response_data.get("action_info")
        assert action_info is not None, "action info cannot be none!"

        # 추후 Multi-Agent를 고려하긴 해야함
        # 각 Agent별로 action정보가 다른 경우도 있을지..?
        if len(action_info) == 1:
            # Single Agent
            key = list(action_info.keys())[0]
            action_type_and_value = list(action_info.values())[0]
            action_value = action_type_and_value[1]
            if self.action_type == "discrete":
                self.action_space = Discrete(len(action_value))
            elif self.action_type == "continuous":
                action_data_type = action_type_and_value[0]
                self.action_space = Box(
                    low=np.float32(action_value[0]),
                    high=np.float32(action_value[1]),
                    shape=(1,),
                    dtype=np.float32,
                )
            else:
                assert False, "action type is not discrte and continuous"
            self.action_key = key
        else:
            # Multi Agent
            raise NotImplementedError

        # self.target_var = ColumnInfo(names=np.array(names), types=np.array(types))

        # Metric Info
        names = []
        types = []
        self.metric_info = dict()
        metric_info = response_data.get("metric_info")
        assert metric_info is not None, "Metric info cannot be none!"
        for metric_name, metric_type in metric_info.items():
            names.append(metric_name)
            types.append(metric_type)
            self.metric_info[metric_name] = dict(
                type=metric_type, weight=None  # TODO: TBD
            )
        self.metric_var = ColumnInfo(names=np.array(names), types=np.array(types))

        # State info
        state_info = response_data.get("state_info")
        assert state_info is not None, "State info cannot be none!"
        obs_range = np.ones(len(state_info)) * np.inf
        self.observation_space = Box(low=-obs_range, high=obs_range, dtype=np.float32)

        # reward
        self.rewarder = create_customized_class_instance(reward_info)

    def seed(self, seed_values):
        request_data = dict()
        request_data["sim_id"] = self._sim_id
        request_data["seed"] = seed_values

        response = rest_post(
            set_seed_url(host=self._host, port=self._port),
            data=json.dumps(request_data),
            timeout=120,
            show_error=True,
        )
        if response is None:
            raise Exception("Cannot set seed simulator")

        return

    def reset(self):
        self._step = 0
        response = rest_post(
            reset_simulator_url(host=self._host, port=self._port),
            data=json.dumps(dict(sim_id=self._sim_id)),
            timeout=120,
            show_error=True,
        )
        if response is None:
            raise Exception("Cannot reset simulator")
        response_data = json.loads(response.text)
        init_state = np.array(response_data.get("init_state"), dtype=np.float32)

        return init_state

    def do_sleep(self):
        if self.sleep > 0:
            sleep_time = random.random() if self.random_sleep else 1
            sleep_time *= self.sleep
            time.sleep(sleep_time)

    def step(self, action):
        self._step += 1

        action = action.item() if action.size == 1 else action
        action_dict = {}
        action_dict[self.action_key] = action
        request_data = dict()
        request_data["sim_id"] = self._sim_id
        request_data["action"] = action_dict
        response = rest_post(
            step_simulator_url(host=self._host, port=self._port),
            data=json.dumps(request_data),
            timeout=500,
            show_error=True,
        )
        if response is None:
            raise Exception("Cannot reset simulator")
        response_data = json.loads(response.text)
        next_state = np.array(response_data.get("next_state"), dtype=np.float32)
        metrics = response_data.get("reward_metrics")
        done = response_data.get("done") or self._step >= self._episode_length

        metric_info = append_to_metric_info(metrics, self.metric_info)

        reward = self.rewarder.compute_reward(
            predicted_action=action_dict,
            target=None,
            metrics=metric_info,
            action_space_info=self.action_space,
            target_weight=None,
            reward_params=None,
        )

        # return
        # obs, rew, done, info
        info = dict()
        return next_state, reward, done, info

    def close(self):
        response = rest_post(
            close_simulator_url(host=self._host, port=self._port),
            data=json.dumps(dict(sim_id=self._sim_id)),
            timeout=120,
            show_error=True,
        )
        if response is None:
            raise Exception("Cannot reset simulator")
        else:
            pass
            # logger.log(f"Simulator is Closed")

    def __del__(self):
        self.close()
