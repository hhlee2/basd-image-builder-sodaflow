from typing import Any

import cloudpickle
import sys
import os
import importlib


class CloudpickleWrapper(object):
    """A cloudpickle wrapper used in SubprocVectorEnv."""

    def __init__(self, data: Any) -> None:
        self.data = data

    def __getstate__(self) -> str:
        return cloudpickle.dumps(self.data)

    def __setstate__(self, data: str) -> None:
        self.data = cloudpickle.loads(data)


def create_customized_class_instance(class_params):
    """Create instance of customized algorithms

    Parameters
    ----------
    class_params: dict
        class_params should contains following keys:
            codeDir: code directory
            classFileName: python file name of the class
            className: class name
            classArgs (optional): kwargs pass to class constructor
    Returns: object
    -------
        Returns customized class instance.
    """

    code_dir = class_params.get("codeDir")
    class_filename = class_params.get("classFileName")
    class_name = class_params.get("className")
    class_args = class_params.get("classArgs")

    if not os.path.isfile(os.path.abspath(os.path.join(code_dir, class_filename))):
        raise ValueError(
            "Class file not found: {}".format(os.path.abspath(os.path.join(code_dir, class_filename)))
        )
    sys.path.append(code_dir)
    module_name = os.path.splitext(class_filename)[0]
    class_module = importlib.import_module(module_name)
    class_constructor = getattr(class_module, class_name)

    if class_args is None:
        class_args = {}
    instance = class_constructor(**class_args)

    return instance


def append_to_metric_info(values, metric_info):
    """
    클래스 내부용 metric info append 함수

    Args:
        values (list) : metric value
        metric_info (dict) : metric information

    Return:
        metric_info (dict) : metric information
    """
    if values is None:
        return None
    else:
        _metric_info = metric_info.copy()
        index = 0
        for k, v in metric_info.items():
            _metric_info[k]["value"] = values[index]
            index += 1

        return _metric_info
