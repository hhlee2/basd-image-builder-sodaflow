import time
import requests

REST_TIME_OUT = 20

API_ROOT_URL = "/api/v1.0/simulator"

NEW_SIMULATOR_API = "/new"

RESET_SIMULATOR_API = "/reset"

CLOSE_SIMULATOR_API = "/close"

STEP_SIMULATOR_API = "/step"

GET_INFO_API = "/get-info"

CHECK_STATUS_API = "/check-status"

SET_SEED_API = "/set-seed"

def rest_post(url, data, timeout, show_error=False):
    '''Call rest post method'''
    try:
        response = requests.post(url, headers={'Accept': 'application/json', 'Content-Type': 'application/json'},
                                 data=data, timeout=timeout)
        return response
    except Exception as exception:
        if show_error:
            print(exception)
        return None

def rest_get(url, timeout, show_error=False):
    '''Call rest get method'''
    try:
        response = requests.get(url, timeout=timeout)
        return response
    except Exception as exception:
        if show_error:
            print(exception)
        return None


def check_status_url(host, port):
    if port is None:
        return "{0}{1}{2}".format(host, API_ROOT_URL, CHECK_STATUS_API)
    else:
        return "{0}:{1}{2}{3}".format(host, port, API_ROOT_URL, CHECK_STATUS_API)


def new_simulator_url(host, port):
    if port is None:
        return "{0}{1}{2}".format(host, API_ROOT_URL, NEW_SIMULATOR_API)
    else:
        return "{0}:{1}{2}{3}".format(host, port, API_ROOT_URL, NEW_SIMULATOR_API)


def reset_simulator_url(host, port):
    if port is None:
        return "{0}{1}{2}".format(host, API_ROOT_URL, RESET_SIMULATOR_API)
    else:
        return "{0}:{1}{2}{3}".format(host, port, API_ROOT_URL, RESET_SIMULATOR_API)


def close_simulator_url(host, port):
    if port is None:
        return "{0}{1}{2}".format(host, API_ROOT_URL, CLOSE_SIMULATOR_API)
    else:
        return "{0}:{1}{2}{3}".format(host, port, API_ROOT_URL, CLOSE_SIMULATOR_API)


def step_simulator_url(host, port):
    if port is None:
        return "{0}{1}{2}".format(host, API_ROOT_URL, STEP_SIMULATOR_API)
    else:
        return "{0}:{1}{2}{3}".format(host, port, API_ROOT_URL, STEP_SIMULATOR_API)


def get_info_url(host, port):
    if port is None:
        return "{0}{1}{2}".format(host, API_ROOT_URL, GET_INFO_API)
    else:
        return "{0}:{1}{2}{3}".format(host, port, API_ROOT_URL, GET_INFO_API)


def check_simulator_server(host, port):
    retry_count = 20
    for _ in range(retry_count):
        response = rest_get(check_status_url(host, port), REST_TIME_OUT)
        if response:
            if response.status_code == 200:
                return True, response
            else:
                return False, response
        else:
            time.sleep(1)

    return False, response


def set_seed_url(host, port):
    if port is None:
        return "{0}{1}{2}".format(host, API_ROOT_URL, SET_SEED_API)
    else:
        return "{0}:{1}{2}{3}".format(host, port, API_ROOT_URL, SET_SEED_API)
