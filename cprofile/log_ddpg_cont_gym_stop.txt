Epoch #1: test_reward: -1347.189814 ± 0.000000, best_reward: -1347.189814 ± 0.000000 in #1
{'best_result': '-1347.19 ± 0.00',
 'best_reward': -1347.189813594789,
 'duration': '28.31s',
 'test_episode': 2,
 'test_speed': '719.00 step/s',
 'test_step': 400,
 'test_time': '0.56s',
 'train_episode': 20,
 'train_speed': '147.58 step/s',
 'train_step': 4096,
 'train_time/collector': '5.14s',
 'train_time/model': '22.62s'}
Final reward: -919.6672936583018, length: 200.0
